import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private userReponsitory: Repository<Member>,
  ) {}
  create(createUserDto: CreateUserDto): Promise<Member> {
    return this.userReponsitory.save(createUserDto);
  }

  findAll(): Promise<Member[]> {
    return this.userReponsitory.find();
  }

  findOne(id: number): Promise<Member> {
    return this.userReponsitory.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.userReponsitory.update(id, updateUserDto);
    const user = await this.userReponsitory.findOneBy({ id });
    return user;
  }

  async remove(id: number) {
    const deleteUser = await this.userReponsitory.findOneBy({ id });
    return this.userReponsitory.remove(deleteUser);
  }
}
