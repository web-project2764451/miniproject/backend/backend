import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MemberService } from './members.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('member')
export class MemberController {
  constructor(private readonly membersService: MemberService) {}
  // Create
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.membersService.create(createUserDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.membersService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.membersService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.membersService.update(+id, updateUserDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.membersService.remove(+id);
  }
}
